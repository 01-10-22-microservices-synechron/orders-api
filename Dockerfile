# Set of commands to create a docker image
# the instructions to convert our application to a docker image
# Base image
FROM openjdk:11-jdk-slim as builder

WORKDIR /app
#copies file/directory from src location to destination location
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

#creates a container
RUN chmod +x ./mvnw && ./mvnw -B dependency:go-offline
#builds an image and disposes the container

# copy will not create a container
COPY src src
#creates a container
RUN ./mvnw package -DskipTests
#builds an image and disposes the container

#creates a container
#RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
#builds an image and disposes the container
#-----------------------------------------#
WORKDIR /source
ARG JAR_FILE=/app/target/orders*.jar
COPY /app/target/orders*.jar application.jar
RUN java -Djarmode=layertools -jar application.jar extract 

#brand new image
FROM openjdk:11.0.13-jre-slim-buster as stage

#argument
ARG DEPENDENCY=/app/target/dependency

# Copy the dependency application file from builder stage artifact
#COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
#COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
#COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

#COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib BOOT-INF/lib
#COPY --from=builder ${DEPENDENCY}/BOOT-INF/classpath.idx BOOT-INF
#COPY --from=builder ${DEPENDENCY}/org org
#COPY --from=builder ${DEPENDENCY}/META-INF META-INF
#COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes BOOT-INF/classes

COPY --from=builder source/dependencies/ ./
COPY --from=builder source/spring-boot-loader/ ./
COPY --from=builder source/snapshot-dependencies/ ./
COPY --from=builder source/application/ ./
# just for development purpose
RUN apt update && apt install -y curl
EXPOSE 8222

#ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.classpath.orders.DemoApplication"]
ENTRYPOINT ["java", "org.springframework.boot.loader.jarLauncher"]