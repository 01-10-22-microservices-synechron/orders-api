package com.classpath.orders.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ApplicationConfiguration {

	@Bean
	@ConditionalOnProperty(prefix = "app", name = "loadUser", havingValue = "true", matchIfMissing = true)
	public User user() {
		return new User();
	}

	@Bean
	@ConditionalOnBean(name = "user")
	public User userBasedOnBean() {
		return new User();
	}

	@Bean
	@ConditionalOnMissingBean(name = "user")
	public User userBasedOnMissingBean() {
		return new User();
	}

	@Bean
	@ConditionalOnMissingClass(value = "com.classpath.orders.DemoApplication")
	public User userBasedOnMissingClass() {
		return new User();
	}

	@Bean
	public WebClient usersClient() {
		return WebClient.builder().baseUrl("https://jsonplaceholder.typicode.com").build();
	}
	
	@Bean
	public WebClient inventoryService() {
		//svc name of inventory-microservice
		return WebClient.builder().baseUrl("http://inventory-service").build();
	}
	@Bean
	public WebClient paymentService() {
		//svc name of inventory-microservice
		return WebClient.builder().baseUrl("http://payment-service").build();
	}
}

class User {

}
