package com.classpath.orders.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor

class DBHealthIndicator implements HealthIndicator {

	private final OrderRepository orderRepository;

	@Override
	public Health health() {
		try {
			this.orderRepository.count();
			return Health.status(Status.UP).withDetail("UP", "application is healthy").build();
		} catch (Exception exception) {
			return Health.status(Status.DOWN).withDetail("DOWN", "application is not healthy").build();
		}
	}
}

@Component
class KafkaHealthIndicator implements HealthIndicator {

	@Override
	public Health health() {
		return Health.status(Status.UP).withDetail("UP", "application is healthy").build();
	}

}

@Component
@RequiredArgsConstructor
@Slf4j
class PaymentGatewayHealthIndicator implements HealthIndicator {

	private final WebClient usersClient;

	@Override
	public Health health() {
		Mono<String> monoResponse = this.usersClient.get().uri("/users").retrieve().bodyToMono(String.class);
		String data = monoResponse.block();
		log.info(data);

		return data != null ? Health.status(Status.UP).withDetail("UP", "application is healthy").build()
				: Health.status(Status.DOWN).withDetail("DOWN", "application is unhealthy").build();
	}
}
