package com.classpath.orders.util;

import static java.util.stream.Stream.of;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ApplicationUtil implements CommandLineRunner {

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void run(String... args) throws Exception {
		//System.out.println("Hello-world from Spring Boot !!");
		
		//imperative style
		/*
		 * String[] beanNames = this.applicationContext.getBeanDefinitionNames(); for
		 * (int index = 0; index < beanNames.length; index++) { if
		 * (beanNames[index].startsWith("user")) { System.out.println(beanNames[index]);
		 * } }
		 */
		
		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		of(beanNames).filter(bean -> bean.startsWith("user")).forEach(System.out::println);
	}
}
