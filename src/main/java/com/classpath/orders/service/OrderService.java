package com.classpath.orders.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpath.orders.dto.OrderDto;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

	private final OrderRepository orderRepository;
	
	private final WebClient inventoryService;
	

	@CircuitBreaker(name="inventoryservice", fallbackMethod = "fallBack")
	@Retry(name="retryConfig")
	@Transactional
	public Order saveOrder(Order order) {
		log.info("Invoking the inventory service " );
		//call the inventory microservice
		
		  ClientResponse clientResponse = this.inventoryService.post()
		  .uri("/api/inventory") .exchange() .block();
		  
		//commit the changes to the DB
		Order savedOrder = this.orderRepository.save(order);
		/*
		 * OrderEvent orderEvent = new OrderEvent(ORDER_ACCEPTED, savedOrder);
		 * Message<OrderEvent> message = MessageBuilder.withPayload(orderEvent).build();
		 * this.source.output().send(message);
		 */return savedOrder;
	}
	
	private Order fallBack(Throwable exception) {
		log.error("Exception while invoking the REST endpoint :: {}", exception.getMessage());
		return Order.builder().build();
	}

	public Map<String, Object> fetchAllOrders(int page, int size, String strDirection, String property) {

		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

		PageRequest pageRequest = PageRequest.of(page, size, direction, property);

		Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

		long totalNumberOfElements = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		List<Order> content = pageResponse.getContent();

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("total-records", totalNumberOfElements);
		responseMap.put("total-pages", totalPages);
		responseMap.put("data", content);

		return responseMap;
	}

	public Order fetchOrderById(long id) {
		return this.orderRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}

	public void deleteOrderById(long id) {
		if (id < 100) {
			this.orderRepository.deleteById(id);
		}
	}

	public Map<String, Object> fetchOrdersByPriceRange(double min, double max, int page, int size, String strDirection,
			String property) {

		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

		PageRequest pageRequest = PageRequest.of(page, size, direction, property);

		Page<OrderDto> pageResponse = this.orderRepository.findByPriceBetween(min, max, pageRequest);

		long totalNumberOfElements = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		List<OrderDto> content = pageResponse.getContent();

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("total-records", totalNumberOfElements);
		responseMap.put("total-pages", totalPages);
		responseMap.put("data", content);

		return responseMap;
	}
}