package com.classpath.orders.exception;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Component
@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Set<String> handleInvalidData(MethodArgumentNotValidException exception) {
		System.out.println("Invalid input data :: ");
		return exception.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toSet());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleInvalidOrder(IllegalArgumentException exception) {
		return new Error(100, exception.getMessage());
		
	}
}

@AllArgsConstructor
@Getter
class Error {
	private int status;
	private String message;
}
