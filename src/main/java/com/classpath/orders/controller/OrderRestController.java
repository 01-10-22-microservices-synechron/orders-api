package com.classpath.orders.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.orders.model.Order;
import com.classpath.orders.service.OrderService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	@GetMapping
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "sort", required = false, defaultValue = "asc") String direction,
			@RequestParam(name = "field", required = false, defaultValue = "customerName") String property){
		return this.orderService.fetchAllOrders(page, size, direction, property);
	}

	@GetMapping("/price")
	public Map<String, Object> fetchAllOrdersByPriceRange(
			@RequestParam(name = "min", required = false, defaultValue = "100") double min,
			@RequestParam(name = "max", required = false, defaultValue = "4000") double max,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "sort", required = false, defaultValue = "asc") String direction,
			@RequestParam(name = "field", required = false, defaultValue = "customerName") String property){
		return this.orderService.fetchOrdersByPriceRange(min, max, page, size, direction, property);
	}
	@GetMapping("/{id}")
	public Order fetchOrderByOrderId(@PathVariable("id") long orderId){
		return this.orderService.fetchOrderById(orderId);
	}
	
	@PostMapping
	@ResponseStatus(CREATED)
	public Order saveOrdr(@RequestBody @Valid Order order){
		return this.orderService.saveOrder(order);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(NO_CONTENT)
	public void deleteOrderByOrderId(@PathVariable("id") long orderId){
		this.orderService.deleteOrderById(orderId);
	}
}