package com.classpath.orders.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

	@Mock
	private OrderRepository orderRepository;

	@InjectMocks
	private OrderService orderService;

	private Order order;

	@BeforeEach
	void setupOrder() {
		order = Order.builder().customerEmail("pradeep@gmail.com").customerName("Pradeep").id(12)
				.orderDate(LocalDate.now()).price(400).build();
	}

	@Test
	void testSaveOrder() {
		/*
		 * Set up all the mock dependencies Set the expectations on the mock object
		 * execute the method under test assert the behavior X verify the the
		 * expectations that are set on the mocks are met X
		 */
		when(orderRepository.save(any())).thenReturn(order);
		Order savedOrder = this.orderService.saveOrder(order);
		assertNotNull(savedOrder);
		assertEquals(12, savedOrder.getId());
		verify(orderRepository, times(1)).save(any());
	}

	@Test
	void testSaveOrderInvalid() {
		/*
		 * Set up all the mock dependencies Set the expectations on the mock object
		 * execute the method under test assert the behavior X verify the the
		 * expectations that are set on the mocks are met X
		 */

		doThrow(new IllegalArgumentException("insufficient data")).when(orderRepository).save(any(Order.class));
		try {
			Order savedOrder = this.orderService.saveOrder(order);
			fail("should not come here..");
		} catch (Exception exception) {
			Assertions.assertNotNull(exception);
			Assertions.assertTrue(exception instanceof IllegalArgumentException);
		}

		verify(orderRepository, times(1)).save(any());
	}

	@Test
	void testInvalidOrderData() {
		doThrow(new IllegalArgumentException("insufficient data")).when(orderRepository).save(any(Order.class));
		assertThrows(IllegalArgumentException.class, () -> this.orderService.saveOrder(order));
		verify(orderRepository, times(1)).save(any());
	}

	@Test
	void testDeleteOrderValid() {
		doNothing().when(orderRepository).deleteById(12L);

		this.orderService.deleteOrderById(12);
		verify(orderRepository, times(1)).deleteById(12L);
	}
	

	@Test
	void testDeleteOrderInValid() {
		//test the void method
		lenient().doNothing().when(orderRepository).deleteById(122L);
		this.orderService.deleteOrderById(122);
		verify(orderRepository, never()).deleteById(122L);
	}

}
